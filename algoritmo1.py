
x = input("""1) Validar si un numero es par. 
2) Mayor de 2 numeros. 
3) Mayor de 3 numeros. 
4) Mayor de n numeros. \n""")
if x == '1':
    x = None
    while type(x) != float:
        try:
            x = float(input('Ingresa un numero:'))
        except Exception as e:
            print(type(e).__name__)
            print("Error. Ingrese un valor numerico")
    else:
        if (x % 2) != 0:
            print('Numero Impar')
        else:
            print('Numero par')
elif x == '2':
    n1 = None
    n2 = None
    while type(n1) != float and type(n2) != float:
        try:
            n1 = float(input('Ingresa numero 1:'))
            n2 = float(input('Ingresa numero 2:'))
        except Exception as e:
            #print(type(e).__name__)
            print("Error. Ingrese un valor numerico")
    else:
        if n1 > n2:
            print(f'{n1} es mayor a {n2}')
        else:
            print(f'{n2} es mayor a {n1}')
elif x == '3':
    n1 = None
    n2 = None
    n3 = None
    while type(n1) != float and type(n2) != float and type(n3) != float:
        try:
            n1 = float(input('Ingresa numero 1:'))
            n2 = float(input('Ingresa numero 2:'))
            n3 = float(input('Ingresa numero 3:'))
        except Exception as e:
            #print(type(e).__name__)
            print("Error. Ingrese un valor numerico")
    else:
        if n1 > n2:
            if n1 > n3:
                print(f'{n1} es mayor que {n2} y {n3}')
            else:
                print(f'{n3} es mayor que {n2} y {n1}')
        else:
            if n2 > n3:
                print(f'{n2} es mayor que {n1} y {n3}')
            else:
                print(f'{n3} es mayor que {n2} y {n1}')
elif x == '4':
    lista = []
    n = None
    mas = False
    men = None
    while not mas:
        try:
            n = float(input('Ingresa un numero : \n'))
        except Exception as e:
            #print(type(e).__name__)
            print("Error. Ingrese un valor numerico \n")
        else:
            lista.append(n)

        f = input('Desea añadir otro numero? a)Si b)No \n')
        if f == 'b':
            mas = True
        else:
            mas = False

    for i in lista:
        if men is None:
            men = i
        else:
            if i > men:
                men = i
    print(f'El numero mayor es {men}')

else:
    print("Valor invalido.")
