#EJERCICIO 3

rsx = False
num = None
while not rsx:
    rps = input("""a) leer un numero. 
b) calcular factorial.
c) determinar si es par.
d) terminar. \n""")
    
    if rps == "a":
        num = input("digite un numero. \n")
        print(f"Su numero es {num} \n")
    elif rps == "b":
        if num is not None:
            nume = int(num)
            ex = 1
            i = 1
            while i <= nume:
                print(ex)
                ex = ex * i
                i += 1
            print(f"El factorial es {ex} \n")
        else:
            print("Por favor digite un numero en la opcion 1) \n")
            
    elif rps == "c":
        if num is not None:
            num = int(num)
            if (num % 2) == 0:
                print(f" {num} es un numero par ")
            else:
                print(f"{num} es un numero impar")
    elif rps == "d":
        print("¡ADIOS!")
        rsx = True
    else:
        print("Opcion invalida..... \n")